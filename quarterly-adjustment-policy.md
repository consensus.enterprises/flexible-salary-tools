This policy describes why and how we adjust our payroll on a quarterly basis to determine our monthly salaries for the next quarter.

Our policies and process for adjusting salary factors *must* align with our **values**.

### Goals

The goals of this policy are to:
* Allow for self-management around work schedules
* Remove any need to get approval for sick days, vacation schedules, etc.
* Provide transparency in how we get paid:
    * This makes our salaries auditable
    * It allows us to reason effectively about our individual salaries, as well as our overall payroll
* Stabilize salaries by flattening out variations in our schedule over the entire quarter.

### Policy

At the beginning of each quarter we collect time-tracking data for all staff for the previous quarter. We then calculate payroll for the new quarter based on efforts over the previous quarter.

We incorporate holidays, vacation and sick days, by removing them from the denominator when calculating average hours per week/month.

We use each staff member's *individual base hourly rate* to calculate their paycheque amount for the quarter.
