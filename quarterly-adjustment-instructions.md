# Instructions for making quarterly salary adjustment

## 1. Gather work time reports for the quarter

For each worker, we review time-tracking reports for the 3 months in the
previous quarter.

* In the `Payroll` sheet for the relevant quarter, enter the hours worked for each month in the yellow cells under columns B through D.
* The tool will calculate a total hours worked in the quarter.

## 2. Validate time off data

In the `Time off` sheet, ensure that each worker's time off is entered
accurately. We empower our team to edit this sheet directly when taking time
off, but you may need to transfer this data in from another system.

The `Time off` sheet has three columns for each worker:

* The `V` column is for vacation days
* The `S` column is for sick ddays
* The `O` column is for other time off

The `Payroll` sheet for the quarter will reflect the total amount of time off
in column G, and subtract this from the calculated total number of work days in
the quarter (column H).

The total days per quarter is calculated based on the `Employees` and
`Holidays` tables in the `Config` sheet. These define each worker's
jurisdiction as well as how many government holidays are applicable in their area.

## 3. Validate average hours/week and hourly rates

Based on the hours worked data combined with the time off data and total days
in the quarter, we calculate an average hours/week in the `Payroll` sheet under
column I. Confirm that these numbers are reasonable, based on historical trend,
expected work schedule and the monthly data entered to the left.

Also ensure each worker's hourly rate is set accurately. Based on this number
and the average hours/week calculation, the tool will show semi-monthly,
monthly, and annual salary numbers.

## 4. Run payroll

Run payroll on the 1st and 15th of the month, and simply use the
semi-monthly amounts for the duration of the next quarter.

If you run payroll at a different interval, the tool can easily be adapted to
calculate bi-weekly or monthly amounts.
