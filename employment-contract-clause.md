# Example employment contract clause

## DISCLAIMER:

**This is not legal advice. You should have a lawyer review this
before incorporating it into any contract.**

<pre>
Our compensation model is based on promoting financial stability while ensuring
maximum flexibility with respect to hours of work. To that end, salaries are
adjusted on a quarterly basis based on hours worked the previous quarter. This
means that, if your hours in one quarter significantly exceed those the
following quarter, you may see a decrease in salary; conversely, if your hours
in one quarter are lower than those the subsequent quarter, you will see an
increase in salary. The particulars of our calculations, and the considerations
applied to compensation are set out in the salary calculation procedure
available upon request. By signing this Agreement, you agree and acknowledge
that your salary may fluctuate from one quarter to the next and agree and
understand that any reduction in salary shall not be construed as a unilateral
change to your employment agreement and does not constitute constructive
dismissal.
</pre>
