# Consensus Flexible Salary Tools

This project contains the tools Consensus Enterprises uses to implement our
[flexible salary adjustment](https://consensus.enterprises/blog/salary-calculator/) process. This includes:

* The tool itself: an OpenDocument spreadsheet which does the heavy lifting of calculating our paycheques every quarter.
* Related documentation and instructions on how to use the tool.
  * [Example employment contract clause](employment-contract-clause.md) - the language we use to enable this process for employees.
  * [Example policy text](quarterly-adjustment-policy.md) - the language we use to describe this organizational policy.
  * [Quarterly adjustment instructions](quarterly-adjustment-instructions.md) - the process for making adjustments each quarter.
  * See [Getting Started](#getting-started) section for more docs.

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

## Project Status

This tool has been in use at [Consensus](https://consensus.enterprises) for
several years, and will iterate as we evolve. This repository will serve as a
template to which we will refer for our internal use, as well as a contribution
to organizations who may find it useful.

## Background

In March of 2022, we published a blog post titled [Consensus Compensation
Calculator](https://consensus.enterprises/blog/salary-calculator/). The post
broadly describes a few aspects of our work designing and building a
compensation model that is truly equitable, transparent, and just.  One aspect
that is mentioned is the quarterly salary adjustment process we use to set our
payroll numbers.

In August of 2022, we were encouraged by organizers of [Drupal Diversity &
Inclusion Camp 2022](https://www.drupaldiversity.com/initiatives/ddi-camp-2022)
to submit a session proposal to share some of the ideas mentioned in the blog
post. We decided to focus on the most mature component, the quarterly salary
adjustment process in our submission.

In September of 2022, we presented [Retiring the traditional 40h work week:
Designing a compensation model for the 21st century](https://talks.consensus.enterprises/ddi-camp-2022),
and published this repository to share the tool at the same time.

**NB:** This methodology relies heavily on the assumption that all workers are
tracking their time in some kind of tool such as [Harvest](https://harvestapp.com) or similar.

## Getting Started

@TODO

* How to make the tool your own
* How to set up a new year
* How to set up a new quarter

* visuals?
* Usage examples?

## Support

Aside from the documentation provided in this repository, Consensus currently
has no official support for this tool. That being said, we are very interested
in sharing our ideas with likeminded organizations!

Feel free to [get in touch](https://consensus.enterprises/#contact) if you'd
like to discuss how this may (or may not work) in your organization.

## Contributing

Feedback welcome! The binary format of the core file here makes it challenging
to integrate external contributions through a typical fork/patch/merge request
process, but that doesn't mean we're unwilling to accept suggestions for
improvement.

Feel free to post an issue on this project, or [contact us](https://consensus.enterprises/#contact)
directly if you have something to contribute.

## Authors & Acknowledgments

* [Christopher Gervais](https://consensus.enterprises/team/christopher) is the primary architect of the tool.
* [Derek Laventure](https://consensus.enterprises/team/derek) is the evangelist, author of the blog post, DDI camp talk, and this repo :)

We would like to acknowledge the wonderful support and encouragement of Fei
Lauren and the Drupal Diversity & Inclusion Camp organizers to share these
ideas more broadly. Thanks!
